#ifndef ARITHMETICBASE_H
#define ARITHMETICBASE_H

#include <types.h>
#include <codingtable.h>

class ArithmeticBase
{
  public:
    ArithmeticBase();

    virtual ~ArithmeticBase() {
    }

  protected:
    enum ZoomType {
      kImpossible, kLeft, kRight, kMiddle
    };

    CodingTable coding_table_;

    Point middle_;
    Point first_quarter_;
    Point third_quarter_;

    void InitializeTable(const CodingTable &coding_table);

    ZoomType DetermineZoomType(Range &range);
    void Zoom(Range &range, ZoomType type);

    virtual void MiddleZoom() = 0;

    virtual void LeftZoom() = 0;

    virtual void RightZoom() = 0;
};

#endif // ARITHMETICBASE_H
