#include "codingtable.h"
#include <cassert>
#include <algorithm>
#include <numeric>

int CodingTable::GetIndexByLetter(Letter letter)
{
  return static_cast<int>(letter);
}

Letter CodingTable::GetLetterByIndex(int index)
{
  return static_cast<Letter>(index);
}

CodingTable::CodingTable(const std::vector<Frequency> &frequencies)
    : scale_(kScale), mapped_letter_range_begins_(kLettersCount + 1)
{
  assert(frequencies.size() == kLettersCount);

  int64_t sum_frequencies = std::accumulate(frequencies.begin(), frequencies.end(), int64_t(0));

  letter_range_begins_.reserve(kLettersCount + 1);
  letter_range_begins_.push_back(0);
  for (auto iter = frequencies.begin(); iter < frequencies.end() - 1; ++iter) {
    Point scaled_frequency = int64_t(scale_) * (*iter) / sum_frequencies;
    letter_range_begins_.push_back(letter_range_begins_.back() + scaled_frequency);
  }

  // Add dummy range begin just for uniformity
  letter_range_begins_.push_back(scale_);
}

CodingTable::LetterSearchResult CodingTable::FindLetterByRangeMapped(
    Range range, Range mapping_image_range) {

  int lower_for_end =
      MappedLowerBound(letter_range_begins_, mapping_image_range, range.end);

  Point letter_begin = MapToRange(letter_range_begins_, mapping_image_range, lower_for_end - 1);
  Point letter_end = MapToRange(letter_range_begins_, mapping_image_range, lower_for_end);

  if (letter_begin <= range.begin) {
    return {true, Range {letter_begin, letter_end},
            GetLetterByIndex(lower_for_end - 1)};
  } else {
    return {false};
  }
}

Range CodingTable::GetLetterRangeMapped(Letter letter, Range mapping_image_range)
{
  int index = GetIndexByLetter(letter);
  Point begin = MapToRange(letter_range_begins_, mapping_image_range, index);
  Point end   = MapToRange(letter_range_begins_, mapping_image_range, index + 1);

  return {begin, end};
}

int CodingTable::MappedLowerBound(const std::vector<Point> &sequence,
                                  Range mapping_image_range, Point point)
{
  int left = 0, right = sequence.size();
  while (left < right - 1) {
    int middle = left + (right - left) / 2;
    if (point > MapToRange(sequence, mapping_image_range, middle)) {
      left = middle;
    } else {
      right = middle;
    }
  }
  if (right == sequence.size()) {
    return right - 1;
  }
  return right;
}

void CodingTable::Write(std::ostream &out_stream)
{
  for (auto iter = letter_range_begins_.begin() + 1;
       iter != letter_range_begins_.end();
       ++iter) {
    Point range_begin = *iter;
    out_stream.write(reinterpret_cast<char *>(&range_begin), sizeof(range_begin));
  }
}

void CodingTable::Read(std::istream &in_stream)
{
  letter_range_begins_.reserve(kLettersCount + 1);
  letter_range_begins_.push_back(0);

  for (int i = 0; i < kLettersCount; ++i) {
    Point range_begin;
    in_stream.read(reinterpret_cast<char *>(&range_begin), sizeof(range_begin));
    letter_range_begins_.push_back(range_begin);
  }
}
