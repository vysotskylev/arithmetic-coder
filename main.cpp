#include <iostream>
#include <fstream>
#include <ios>
#include <cstring>

#include <arithmeticcoder.h>
#include <arithmeticdecoder.h>

using std::cout;
using std::endl;

void PrintUsage(const char *prog_name) {
  cout << "Usage: " << prog_name << " <-e|-d> <in_file>  <out_file>" << endl;
}

int main(int argc, char **argv)
{
  if (argc < 4) {
    PrintUsage(argv[0]);
    return 1;
  }

  std::string direction(argv[1]);

  if (direction == "-e") {
    std::ifstream in_stream(argv[2]);
    std::ofstream out_stream(argv[3], std::ios_base::binary);
    ArithmeticCoder coder(in_stream, out_stream);
    coder.Encode();
  } else if (direction == "-d") {
    std::ifstream in_stream(argv[2], std::ios_base::binary);
    std::ofstream out_stream(argv[3]);
    ArithmeticDecoder decoder(in_stream, out_stream);
    decoder.Decode();
  } else {
    cout << "Unknown option " << direction << endl;
    PrintUsage(argv[0]);
    return 1;
  }


  return 0;
}

