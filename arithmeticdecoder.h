#ifndef ARITHMETICDECODER_H
#define ARITHMETICDECODER_H

#include <ios>

#include <types.h>
#include <codingtable.h>
#include <bitstream.h>
#include <arithmeticbase.h>

class ArithmeticDecoder : ArithmeticBase
{
  public:
    ArithmeticDecoder(std::istream &in_stream, std::ostream &out_stream);

    void Decode();

  private:
    Range current_range_;
    Range previous_letter_range_;
    int times_can_divide_;

    Frequency letters_read_;
    Frequency message_length_;

    std::istream &in_stream_;
    std::ostream &out_stream_;

    InBitStream bit_stream_;

    void ExtractLetters();


    virtual void MiddleZoom() {
    }

    virtual void LeftZoom() {
    }

    virtual void RightZoom() {
    }

};

#endif // ARITHMETICDECODER_H
