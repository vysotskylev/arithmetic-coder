#ifndef BITSTREAM_H
#define BITSTREAM_H

#include <ios>
#include <cstdint>
#include <iostream>

class OutBitStream
{
  public:
    OutBitStream(std::ostream &out_stream)
        : out_stream_(out_stream), bit_index_(0), buffer_(0) {
    }

    OutBitStream &operator<<(bool bit) {
      buffer_ |= (bit << bit_index_);
      bit_index_++;
      if (bit_index_ == sizeof(buffer_) * 8) {
        Flush();
        bit_index_ = 0;
        buffer_ = 0;
      }

      return *this;
    }

    ~OutBitStream() {
      if (bit_index_ > 0) { // Something has been written
        Flush();
      }
    }

  private:
    std::ostream &out_stream_;
    int bit_index_;
    uint8_t buffer_;

    void Flush() {
      out_stream_.write(reinterpret_cast<char *>(&buffer_), sizeof(buffer_));
    }
};


class InBitStream {
  public:
    InBitStream(std::istream &in_stream)
        : in_stream_(in_stream), bit_index_(kBitsInBuffer), buffer_(0), eof_(false) {
    }

    InBitStream &operator>>(bool &bit) {
      if (bit_index_ == kBitsInBuffer) {
        if (!in_stream_.read(reinterpret_cast<char *>(&buffer_), sizeof(buffer_))) {
          eof_ = true;
        }
        bit_index_ = 0;
      }

      bit = buffer_ & (1 << bit_index_);
      ++bit_index_;

      return *this;
    }

    operator bool() {
      return !eof_;
    }

  private:
    std::istream &in_stream_;
    int bit_index_;
    uint8_t buffer_;
    bool eof_;

    static const int kBitsInBuffer = sizeof(buffer_) * 8;
};

#endif // BITSTREAM_H
