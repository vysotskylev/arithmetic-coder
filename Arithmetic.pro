TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11

SOURCES += main.cpp \
    codingtable.cpp \
    arithmeticcoder.cpp \
    arithmeticdecoder.cpp \
    arithmeticbase.cpp

HEADERS += \
    codingtable.h \
    types.h \
    arithmeticcoder.h \
    bitstream.h \
    arithmeticdecoder.h \
    arithmeticbase.h

