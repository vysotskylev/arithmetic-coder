#include "arithmeticbase.h"

ArithmeticBase::ArithmeticBase()
{
}

void ArithmeticBase::InitializeTable(const CodingTable &coding_table)
{
  coding_table_ = coding_table;

  middle_ = coding_table_.GetScale() / 2;
  first_quarter_ = middle_ / 2;
  third_quarter_ = middle_ + middle_ / 2;
}

ArithmeticBase::ZoomType ArithmeticBase::DetermineZoomType(Range &range)
{
  if (range.end <= middle_) {
    return kLeft;
  }
  if (range.begin >= middle_) {
    return kRight;
  }
  if (range.end <= third_quarter_ && range.begin >= first_quarter_){
    return kMiddle;
  }

  return kImpossible;
}

void ArithmeticBase::Zoom(Range &range, ZoomType type)
{
  switch (type) {
    case kLeft:
      LeftZoom();
      range.end = range.end * 2;
      range.begin = range.begin * 2;
      break;
    case kRight:
      RightZoom();
      range.end = (range.end - middle_) * 2;
      range.begin = (range.begin - middle_) * 2;
      break;
    case kMiddle:
      MiddleZoom();
      range.begin = (range.begin - first_quarter_) * 2;
      range.end   = (range.end   - first_quarter_) * 2;
      break;
  }
}
