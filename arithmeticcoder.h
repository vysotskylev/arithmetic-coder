#ifndef ARITHMETICCODER_H
#define ARITHMETICCODER_H

#include <ios>

#include <types.h>
#include <codingtable.h>
#include <bitstream.h>
#include <arithmeticbase.h>

class ArithmeticCoder : public ArithmeticBase
{
  public:
    ArithmeticCoder(std::istream &in_stream, std::ostream &out_stream);

    void Encode();

  private:
    Range current_range_;
    Frequency message_length_;

    int middle_zoom_;

    std::istream &in_stream_;
    std::ostream &out_stream_;

    OutBitStream bit_stream_;

    void MiddleZoom();
    void LeftZoom();
    void RightZoom();

    void FlushMiddleZoom(bool bit);

    CodingTable BuildCodingTable();
    void EncodeLetter(Letter letter);
    void CodeToEnd();
};

#endif // ARITHMETICCODER_H
