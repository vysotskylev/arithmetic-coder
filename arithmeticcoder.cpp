#include "arithmeticcoder.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>


ArithmeticCoder::ArithmeticCoder(std::istream &in_stream, std::ostream &out_stream)
    : in_stream_(in_stream), out_stream_(out_stream), bit_stream_(out_stream),
      middle_zoom_(0)
{
}

void ArithmeticCoder::Encode() {
  InitializeTable(BuildCodingTable());

  current_range_ = Range {0, coding_table_.GetScale()};

  // Reserve place for length;
  out_stream_.write(reinterpret_cast<char *>(&message_length_), sizeof(message_length_));

  coding_table_.Write(out_stream_);

  Letter letter;
  while (in_stream_.get(letter)) {
    EncodeLetter(letter);
  }
  CodeToEnd();
}

void ArithmeticCoder::MiddleZoom()
{
  ++middle_zoom_;
}

void ArithmeticCoder::LeftZoom()
{
  FlushMiddleZoom(0);
}

void ArithmeticCoder::RightZoom()
{
  FlushMiddleZoom(1);
}

inline void ArithmeticCoder::FlushMiddleZoom(bool bit)
{
  bit_stream_ << bit;
  for (int i = 0; i < middle_zoom_; ++i) {
    bit_stream_ << !bit;
  }
  middle_zoom_ = 0;
}



CodingTable ArithmeticCoder::BuildCodingTable() {
  std::vector<Frequency> frequencies(CodingTable::kLettersCount);

  Letter letter;
  while (in_stream_.get(letter)) {
    ++frequencies[CodingTable::GetIndexByLetter(letter)];
  }

  message_length_ = std::accumulate(frequencies.begin(), frequencies.end(), 0);

  in_stream_.clear();
  in_stream_.seekg(0);

  return CodingTable(frequencies);
}



void ArithmeticCoder::EncodeLetter(Letter letter) {
  using namespace std;

  current_range_ = coding_table_.GetLetterRangeMapped(letter, current_range_);

  ZoomType zoom_type;
  while ((zoom_type = DetermineZoomType(current_range_))) {
    Zoom(current_range_, zoom_type);
  }
}

void ArithmeticCoder::CodeToEnd()
{
  if (current_range_.end > third_quarter_) {
    FlushMiddleZoom(1);
    bit_stream_ << 0;
  } else {
    FlushMiddleZoom(0);
    bit_stream_ << 1;
  }
}
