#ifndef TYPES_H
#define TYPES_H

using Frequency = int;

using Point = int;

using Letter = char;

struct Range {
    Point begin, end;
};

struct LetterRange {
    Letter letter;
    Range range;
};

#endif // TYPES_H
