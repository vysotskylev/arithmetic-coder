#ifndef CODINGTABLE_H
#define CODINGTABLE_H

#include <vector>
#include <stdexcept>
#include <iostream>

#include "types.h"

class CodingTable
{
  public:
    struct LetterSearchResult {
      bool success;
      Range letter_range;
      Letter letter;
    };

    static const int kLettersCount = 1 << (8 * sizeof(Letter));
    static const int kBitsInScale = 30;
    static const Point kScale = 1 << kBitsInScale;

    static int GetIndexByLetter(Letter letter);
    static Letter GetLetterByIndex(int index);

    CodingTable(const std::vector<Frequency> &frequencies);

    CodingTable() : scale_(kScale), mapped_letter_range_begins_(kLettersCount + 1) {
    }

    LetterSearchResult FindLetterByRangeMapped(Range range, Range mapping_image_range);

    Range GetLetterRangeMapped(Letter letter, Range mapping_image_range);


    inline Point MapToRange(const std::vector<Point> &sequence, Range mapping_image_range,
                            int index) {
      Point range_size = mapping_image_range.end - mapping_image_range.begin;
      Point new_range_size = range_size - sequence.size() + 1;

      if (new_range_size <= 0) {
        throw std::runtime_error("Too small range given to CodingTable::GetLetterRangeMapped");
      }

      if (index == sequence.size() - 1) {
        return mapping_image_range.end;
      }

      return mapping_image_range.begin + int64_t(sequence[index]) * new_range_size /
             (sequence.back() - sequence.front()) + index;
    }

    int MappedLowerBound(const std::vector<Point> &sequence, Range mapping_image_range,
                         Point point);

    Point GetScale() {return scale_;}

    void Write(std::ostream &out_stream);

    void Read(std::istream &in_stream);

  private:
    std::vector<Point> letter_range_begins_;
    std::vector<Point> mapped_letter_range_begins_;

    Point scale_;
};

#endif // CODINGTABLE_H
