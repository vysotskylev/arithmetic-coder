#include "arithmeticdecoder.h"
#include <stdexcept>

ArithmeticDecoder::ArithmeticDecoder(std::istream &in_stream, std::ostream &out_stream)
    : in_stream_(in_stream), out_stream_(out_stream), bit_stream_(in_stream)
{
}

void ArithmeticDecoder::Decode()
{
  using namespace std;

  in_stream_.read(reinterpret_cast<char *>(&message_length_), sizeof(message_length_));
  letters_read_ = 0;

  CodingTable table;
  table.Read(in_stream_);
  InitializeTable(table);

  times_can_divide_ = CodingTable::kBitsInScale;

  current_range_ = Range {0, coding_table_.GetScale()};
  previous_letter_range_ = current_range_;

  bool bit;
  while (bit_stream_ >> bit) {
    Point middle = current_range_.begin + (current_range_.end - current_range_.begin) / 2;
    if (bit) {
      current_range_.begin = middle;
    } else {
      current_range_.end   = middle;
    }

    --times_can_divide_;
    if (times_can_divide_ == 0) {
      ExtractLetters();
      if (times_can_divide_ == 0) {
        throw std::runtime_error("Cannot zoom. Decoding failed");
      }
    }
  }
  ExtractLetters();
  if (letters_read_ < message_length_) {
    std::cerr << "Stream ended before all letters were read (read " << letters_read_ << ")" << std::endl;
  }
}

void ArithmeticDecoder::ExtractLetters()
{
  while (true) {
    if (letters_read_ >= message_length_) {
      return;
    }
    CodingTable::LetterSearchResult search_result =
        coding_table_.FindLetterByRangeMapped(current_range_, previous_letter_range_);

    if (search_result.success) {
      previous_letter_range_ = search_result.letter_range;

      ZoomType zoom_type;
      while ((zoom_type = DetermineZoomType(previous_letter_range_)) != kImpossible) {
        Zoom(previous_letter_range_, zoom_type);
        Zoom(current_range_, zoom_type);
        ++times_can_divide_;
      }

      out_stream_.put(search_result.letter);
      ++letters_read_;
    } else {
      break;
    }
  }
}


